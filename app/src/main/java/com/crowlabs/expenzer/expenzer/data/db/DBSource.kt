package com.crowlabs.expenzer.expenzer.data.db

import com.crowlabs.expenzer.expenzer.data.models.details
import com.crowlabs.expenzer.expenzer.data.models.topics

interface DBSource {
    fun saveTitle(title: String)

    fun saveDetails(title: String, category: String, date: String, month: String,
                    description: String, amount: String)

    fun deleteTitle(title: String): Boolean

    fun deleteDetails(description: String): Boolean

    fun retrieveTitle(): ArrayList<topics>

    fun retrieveDetails(): ArrayList<details>

    fun retrieveDetails(category: String): ArrayList<details>

    fun retrieveDetailsMonth(month: String): ArrayList<details>
}