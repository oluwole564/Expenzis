package com.crowlabs.expenzer.expenzer.data.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.crowlabs.expenzer.expenzer.utils.Extras.AMOUNT
import com.crowlabs.expenzer.expenzer.utils.Extras.CATEGORY
import com.crowlabs.expenzer.expenzer.utils.Extras.DATE
import com.crowlabs.expenzer.expenzer.utils.Extras.DESCRIPTION
import com.crowlabs.expenzer.expenzer.utils.Extras.DETAILS
import com.crowlabs.expenzer.expenzer.utils.Extras.DETAILS_TABLE
import com.crowlabs.expenzer.expenzer.utils.Extras.MONTH
import com.crowlabs.expenzer.expenzer.utils.Extras.TITLE
import com.crowlabs.expenzer.expenzer.utils.Extras.TITLE_TABLE
import com.crowlabs.expenzer.expenzer.utils.Extras.TOPIC
import org.jetbrains.anko.db.*

/**
 * Created by yung on 10/19/17.
 */
class MySQLHelper(ctx: Context): ManagedSQLiteOpenHelper(ctx, "Expenziz") {
    companion object {
        private var instance: MySQLHelper? = null

        fun getInstance(ctx: Context): MySQLHelper {
            if(instance == null){
                instance = MySQLHelper(ctx.applicationContext)
            }
            return instance as MySQLHelper
        }
    }

    override fun onCreate(p0: SQLiteDatabase?) {

        p0?.createTable(TITLE_TABLE, true, TOPIC to TEXT)
        p0?.createTable(DETAILS_TABLE, true, TITLE to TEXT, DESCRIPTION to TEXT,
                MONTH to TEXT, AMOUNT to TEXT, CATEGORY to TEXT, DATE to TEXT)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}