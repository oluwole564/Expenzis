package com.crowlabs.expenzer.expenzer.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.crowlabs.expenzer.expenzer.R
import com.crowlabs.expenzer.expenzer.home.HomeActivity

class SplashActivity : AppCompatActivity(), SplashView.View {

    lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        presenter = SplashPresenter(this)
        presenter.startTimer()
    }

    override fun launchHome() {
        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
    }
}
