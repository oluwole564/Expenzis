package com.crowlabs.expenzer.expenzer.data.models

data class details (val title: String, val description: String, val month: String,
                    val amount: String, val category: String, val date: String)