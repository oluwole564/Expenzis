package com.crowlabs.expenzer.expenzer.new_

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.crowlabs.expenzer.expenzer.R

class NewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)
    }
}
