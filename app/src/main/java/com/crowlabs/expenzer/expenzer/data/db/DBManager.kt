package com.crowlabs.expenzer.expenzer.data.db

import android.content.Context
import android.util.Log
import com.crowlabs.expenzer.expenzer.data.models.details
import com.crowlabs.expenzer.expenzer.data.models.topics
import com.crowlabs.expenzer.expenzer.utils.Extras.AMOUNT
import com.crowlabs.expenzer.expenzer.utils.Extras.CATEGORY
import com.crowlabs.expenzer.expenzer.utils.Extras.DATE
import com.crowlabs.expenzer.expenzer.utils.Extras.DESCRIPTION
import com.crowlabs.expenzer.expenzer.utils.Extras.DETAILS_TABLE
import com.crowlabs.expenzer.expenzer.utils.Extras.MONTH
import com.crowlabs.expenzer.expenzer.utils.Extras.TITLE
import com.crowlabs.expenzer.expenzer.utils.Extras.TITLE_TABLE
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DBManager(context: Context): DBSource {

    var database: MySQLHelper = MySQLHelper.getInstance(context)

    private val TAG = this.javaClass.simpleName

    override fun deleteDetails(description: String): Boolean {
        var deleted = false
        database.use {
            try{
                beginTransaction()
                val result = delete(DETAILS_TABLE, "$DESCRIPTION = {description}",
                        "description" to description) > 0
                deleted = if(result){
                    setTransactionSuccessful()
                    true
                }
                else{
                    false
                }
            }
            catch (e: Exception){
                e.printStackTrace()
                Log.e(TAG, e.message)
                deleted = false
            }
            finally {
                endTransaction()
            }
        }
        return deleted
    }

    override fun deleteTitle(title: String): Boolean {
        var deleted = false
        database.use {
            try {
                val result = delete(TITLE_TABLE, "$TITLE = {title}",
                        "title" to title) > 0
                deleted = if(result){
                    setTransactionSuccessful()
                    true
                }
                else{
                    false
                }
            }
            catch (e: Exception){
                e.printStackTrace()
                Log.e(TAG, e.message)
                deleted = false
            }
            finally {
                endTransaction()
            }
        }
        return deleted
    }

    override fun retrieveDetails(): ArrayList<details> {
        return database.use {
            val parser = classParser<details>()
            select(DETAILS_TABLE).parseList(parser).toCollection(ArrayList())
        }
    }

    override fun retrieveDetails(category: String): ArrayList<details> {
        return database.use {
            val parser = classParser<details>()
            select(DETAILS_TABLE).whereSimple("$CATEGORY = ?", category)
                    .parseList(parser).toCollection(ArrayList())
        }
    }

    override fun retrieveDetailsMonth(month: String): ArrayList<details> {
        return database.use {
            val parser = classParser<details>()
            select(DETAILS_TABLE).whereSimple("$MONTH = ?", month)
                    .parseList(parser).toCollection(ArrayList())
        }
    }

    override fun retrieveTitle(): ArrayList<topics> {
        return database.use {
            val parser = classParser<topics>()
            select(TITLE_TABLE).parseList(parser).toCollection(ArrayList())
        }
    }

    override fun saveDetails(title: String, category: String, date: String, month: String,
                             description: String, amount: String) {
        database.use {
            insert(DETAILS_TABLE, TITLE to title, CATEGORY to category, DATE to date,
                    MONTH to month, DESCRIPTION to description, AMOUNT to amount)
        }
    }

    override fun saveTitle(title: String) {
        database.use {
            insert(TITLE_TABLE, TITLE to title)
        }
    }
}