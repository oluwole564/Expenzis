package com.crowlabs.expenzer.expenzer.home


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crowlabs.expenzer.expenzer.R


/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {
    lateinit var ctx: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root_view = inflater.inflate(R.layout.fragment_home, container, false)
        ctx = root_view.context
        // Inflate the layout for this fragment
        return root_view
    }


}
