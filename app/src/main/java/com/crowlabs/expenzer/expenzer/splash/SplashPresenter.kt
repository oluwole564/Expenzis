package com.crowlabs.expenzer.expenzer.splash

class SplashPresenter(var view: SplashView.View): SplashView.Action {

    override fun startTimer() {
        val timer = object : Thread() {
            override fun run() {
                try{
                    sleep(3000)
                }
                catch (e: Exception){
                    e.printStackTrace()
                }
                finally {
                    view.launchHome()
                }
            }
        }
        timer.start()
    }
}